//
// Created by eoghanconlon73 on 08/11/23.
//

#ifndef CE4703_2023_UTILS_H
#define CE4703_2023_UTILS_H

#include <stdlib.h>
#include <time.h>

int randomPositiveInteger();

#endif //CE4703_2023_UTILS_H
